<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Information Form</title>

    <link rel="stylesheet" href="../..resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../..resource/bootstrap/css/bootstrap-theme.min.css">
    <script rel="stylesheet" href="../..resource/bootstrap/jss/bootstrap.jss"></script>

    <link rel="stylesheet" href="../../resource/style.css">

</head>

<body>

<?php

require_once("../../src/BITM/131432/Course.php");
require_once("../../src/BITM/131432/Student.php");
$objStudent = new \App\Student();

$objStudent->setName($_POST{"name"});
$objStudent->setStudentID($_POST{"studentID"});

$objCourse = new \App\Courses();

$objCourse->setMarkBangla($_POST["markBangla"]);
$objCourse->setGradeBangla();

$objCourse->setMarkEnglish($_POST["markEnglish"]);
$objCourse->setGradeEnglish();

$objCourse->setMarkMath($_POST["markMath"]);
$objCourse->setGradeMath();


$name = $objStudent->getName();
$studentID = $objStudent->getStudentID();

$markBangla= $objCourse->getMarkBangla();
$gradeBangla= $objCourse->getGradeBangla();

$markEnglish= $objCourse->getMarkEnglish();
$gradeEnglish= $objCourse->getGradeEnglish();

$markMath= $objCourse->getMarkMath();
$gradeMath= $objCourse->getGradeMath();

$resultPage=<<<RESULT

?>

    <div class='studentInformation'>

        <table class="table">
            <tr>
                <td>Subject</td>
                <td>Mark Obtained</td>
                <td>Grade Obtained</td>
            </tr>

            <tr>
                <td>Bangla</td>
                <td>$markBangla</td>
                <td>$gradeBangla</td>
            </tr>

            <tr>
                <td>Subject</td>
                <td>Mark Obtained</td>
                <td>Grade Obtained</td>
            </tr>

            <tr>
                <td>English</td>
                <td>$markEnglish</td>
                <td>$gradeEnglish</td>
            </tr>

            <tr>
                <td>Subject</td>
                <td>Mark Obtained</td>
                <td>Grade Obtained</td>
            </tr>

            <tr>
                <td>Math</td>
                <td>$markMath</td>
                <td>$gradeMath</td>
            </tr>

        </table>

    </div>

RESULT;

echo $resultPage;

file_put_contents("result")


?>