<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Information Form</title>

    <link rel="stylesheet" href="../..resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../..resource/bootstrap/css/bootstrap-theme.min.css">
    <script rel="stylesheet" href="../..resource/bootstrap/jss/bootstrap.jss"></script>

    <link rel="stylesheet" href="../../resource/style.css">

</head>
<body>

    <div class="container">
        <form action="process.php" method="post">

        <div class="form-group">
            <label for="name">Student Name:</label>
            <input type="text" class="form-group" name="name" placeholder="Please Enter Student's Name Here...">
        </div>

        <div class="form-group">
            <label for="name">Student ID:</label>
            <input type="text" class="form-group" name="name" placeholder="Please Enter Student's ID Here...">
        </div>

    <div class="studentMark">

      <div class="form-group">
        <label for="markBangla">Bangla Mark:</label>
        <input type="number" step="any" class="form-control" name="markBangla" placeholder="Please Enter Student's Mark Here...">
      </div>

        <div class="form-group">
            <label for="markEnglish">English Mark:</label>
            <input type="number" step="any" class="form-control" name="markEnglish" placeholder="Please Enter Student's Mark Here...">
        </div>

        <div class="form-group">
            <label for="markMath">Math Mark:</label>
            <input type="number" step="any" class="form-control" name="markMath" placeholder="Please Enter Student's Mark Here...">
        </div>

        <input type="submit">

    </div>

        </form>

    </div>

</body>
</html>